# -*- coding: utf-8 -*-
# ***************************************************************************
# start_kostra.py  -   Part of KOSTRA-Tools, Design Floods with KOSTRA/Rewanus and ArcEGMO for QGIS
#                      Developed for and funded by LHW Saxony-Anhalt
# ---------------------
#     begin                : 24.06.2019
#     copyright            : (C) 2019 by Ruben Müller, Büro für Angewandte Hydrologie, Berlin
#     email                : info at ruben.mueller at bah-berlin dot de
#
# ***************************************************************************
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU General Public License as published by  *
# *   the Free Software Foundation; either version 2 of the License, or     *
# *   (at your option) any later version.                                   *
# *                                                                         *
# ***************************************************************************

import os
import shutil
import time
import gzip
import pickle
import sys
import datetime
import codecs
import matplotlib.pyplot as plt
import numpy as np
import subprocess

from matplotlib import gridspec

from qgis.core import Qgis
from qgis.core import QgsVectorLayer
from qgis.core import QgsProject
from qgis.utils import iface
from qgis.PyQt.QtCore import Qt
from qgis.PyQt.QtWidgets import QDialog
from qgis.PyQt.QtWidgets import QButtonGroup
from qgis.PyQt.QtWidgets import QFileDialog
from qgis.PyQt.QtWidgets  import QAbstractButton
from qgis.PyQt.QtWidgets  import qApp
from qgis.PyQt.QtCore import QSignalMapper
from qgis.PyQt.QtCore import pyqtSignal
from qgis.PyQt.QtCore import QThread
from qgis.PyQt.QtCore import pyqtSlot
from qgis.PyQt.QtCore import QObject


from .pyqtDialogs.dialog_kostra import Ui_Dialog as Ui_Kostra
from .start_fgw import Start_dialog_fgw_tool

from .setup import setup__ as SETUP

DEBUG = SETUP.DEBUG

SETTINGTIME = True
### this settings must match the starting date in the arc_egmo.ste
SETTINGTIME_DTD = datetime.datetime(1990, 4, 1, 0, 0)

SETTINGTIME_DT = datetime.datetime.strftime(SETTINGTIME_DTD, '%d.%m.%Y %H:%M')
TIMEDELTA = datetime.timedelta(seconds=5577200000)


HQT = [1, 2, 3, 5, 10, 20, 25, 30, 50, 100]
DECODER= 'ISO-8859-1'


def check_layer_loaded(layer_name):
    '''check if layer with layer_name is loaded

    Parameters
    ----------
    layer_name : str
        the name of the layer

    Returns
    -------
    tmp : bool
        True if layer is registered''',
    boolean = False
    layers = QgsProject.instance().mapLayers()
    for name, layer in layers.items():
        if layer.name() == (layer_name):
            boolean = True
            break
    return boolean


def load_layer(layer_name, set_active=False, folderx=None, altName=None):
    '''load an shape layer_name

    Parameters
    ----------
    layer_name : str
        the name of the layer
    set_active : bool, optional
        and set the loaded layer active if True
    folderx : str, optional
        provide an alternative path to the ESRI layer file
    altName : str, optional
        provide an alternative layer name when loaded

    Returns
    -------
    layer : layer object
        the loaded layer'''

    if folderx is None:
        folderx = SETUP.BFOLDER_GIS
    if altName is None:
        altName = layer_name
    try:
        if not check_layer_loaded(altName):
            layer = QgsVectorLayer(os.path.join(folderx, layer_name + ".shp"), altName, "ogr")
            QgsProject.instance().addMapLayer(layer)
        else:
            layer = QgsProject.instance().mapLayersByName(altName)[0]
        if set_active:
            iface.setActiveLayer(layer)
        return layer
    except:
        iface.messageBar().pushMessage('Konnte Layer {} nicht laden.'.format(layer_name),
                                        level=Qgis.Warning, duration=7)

def load_layers():
    '''load some layers, if they are not already'''
    load_layer(os.path.join(SETUP.BFOLDER_GIS, SETUP.TG_LAYER), set_active=False, altName=SETUP.TG_LAYER)
    load_layer(os.path.join(SETUP.BFOLDER_GIS, SETUP.KOSTRA_LAYER), set_active=False, altName=SETUP.KOSTRA_LAYER)
    load_layer(os.path.join(SETUP.BFOLDER_GIS, SETUP.REWANUS_LAYER), set_active=False, altName=SETUP.REWANUS_LAYER)
    load_layer(os.path.join(SETUP.BFOLDER_GIS, SETUP.FGW_LAYER), set_active=True, altName=SETUP.FGW_LAYER)


def remove_file_ending(folder, ending):
    '''delete files with a certain ending'''
    [os.remove(os.path.join(folder, filex)) for filex in os.listdir(folder) if filex.endswith(ending)]


def remove_file_starts(folder, start):
    '''delete files starting with a certain pattern'''
    [os.remove(os.path.join(folder, filex)) for filex in os.listdir(folder) if filex.startswith(start)]


def clean_folder(basefolder):
    '''delete all KOSTRA_txxx folders in results'''
    for folder in os.listdir(basefolder):
        if os.path.isdir(os.path.join(basefolder, folder)):
            shutil.rmtree(os.path.join(basefolder, folder))


def arcegmo_qc(results_fileP):
    '''
    reads the simulation results in excel mode

    Parameters
    ----------
    control: dict
        the dictionary containing the arc_egmo.ste
    results_file : string
        override for fgw_mit.qc
    timestep : str
        override for time step in control

    Returns
    -------
    dict
        the dictionary contains the time series of all FGW in the results file
        and the date times
    '''
    if SETTINGTIME == False:
        runin_over = True
    else:
        runin_over = False

    ### number of lines in file
    header = {}
    i = 0
    linecount = 0
    with open(results_fileP, 'r') as fr:
        for line in fr:
            temp = line.split()
            if SETTINGTIME_DT == temp[0][1:] + " "+ temp[1][0:-1] and not runin_over:
                runin_over = True

            if runin_over:
                linecount = linecount+1

    ### reset
    if SETTINGTIME == False:
        runin_over = True
    else:
        runin_over = False

    with open(results_fileP, 'r') as fr:
        #print('linecount ' + str(linecount))
        for ik, line in enumerate(fr):
            if ik == 0:
                h = line.split()
                header.update({h[0]:[]})
                for j in range(0, len(h)):  ### len(h)-1 for some reason???
                    header.update({h[j]: np.zeros([linecount, 1])})
            else:
                temp = line.split()
                if len(temp) == 0:
                    break
                if SETTINGTIME_DT == temp[0][1:] + " "+ temp[1][0:-1] and not runin_over:
                    runin_over = True
                    i = 0
                if runin_over:
                    header[h[0]][i] = (datetime.datetime.strptime(temp[0][1:] \
                        + " "+ temp[1][0:-1], '%d.%m.%Y %H:%M') + TIMEDELTA).timestamp()

                    for j in range(2, len(h)+1):
                        #temp[j]
                        header[h[j-1]][i] = float(temp[j])
                    i += 1
    return header


def plot_detail(t, sP, sQ, title):
    '''plot for a combination of HQT and duration'''
    if DEBUG: print('plot_detail', t, sP, sQ, title)
    plt.figure()
    gs = gridspec.GridSpec(2, 1, height_ratios=[1, 2])

    # HYDROGRAM CHART
    ax = plt.subplot(gs[1])
    ax.plot(t,sQ)
    ax.set_ylabel(u'Q [m³/s]', color='b', size=14)
    ax.set_xlabel('Zeit (Stunde)', size=14)

    ax.tick_params(axis='y', colors='b')
    ax.xaxis.grid(b=True, which='major', color='.7', linestyle='-')
    ax.yaxis.grid(b=True, which='major', color='.7', linestyle='-')
    ax.set_xlim(min(t), max(t))
    ax.set_ylim(0, max(sQ)*1.2)

    # PRECIPITATION/HYETOGRAPH CHART
    ax2 = plt.subplot(gs[0])
    ax2.bar(t, sP, 1, color='b')#'#b0c4de'
    ax2.xaxis.grid(b=True, which='major', color='.7', linestyle='-')
    ax2.yaxis.grid(b=True, which='major', color='0.7', linestyle='-')
    ax2.set_ylabel('P(mm)', size=14)
    ax2.set_xlim(min(t), max(t))
    ax2.set_title(title, size=14)
    plt.setp(ax2.get_xticklabels(), visible=False)

    plt.tight_layout()
    ax2.invert_yaxis()
    plt.gcf().subplots_adjust(bottom=0.15)
    xspacing = np.linspace(0, len(t), 12)
    ax.set_xticks(xspacing)
    ax.set_xticklabels([str(int(x*15/60)) for x in xspacing])
    ax2.set_xticks(xspacing)
    ax2.set_xticklabels([str(int(x*15/60)) for x in xspacing])
    plt.show()


def save_obj_compressed(name, obj):
    '''
    saves a struct or list into a prickle binary file

    Parameters
    ----------
    name : string
        path and filename for prickle file
        without file ending, functions add .pkl
    obj : struct, list,...
        object to save in prickle file
    '''
    if len(name) > 6:
        if name[-5:] == '.klhw':
            name = name[:-5]
    try:
        with gzip.open(name + '.klhw', 'wb') as f:
            pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)
            #print('saved')
    except:
        raise IOError('could not write prickle file {}'.format(name))


def load_obj_compressed(name):
    '''
    loads a prickle binary file

    Parameters
    ----------
    name : string
        path and filename for prickle file
    '''
    if len(name) > 6:
        if name[-5:] == '.klhw':
            name = name[:-5]
    try:
        with gzip.open(name + '.klhw', 'rb') as f:
            if sys.version_info[0] < 3:
                temp = pickle.load(f)
            else:
                temp = pickle.load(f, encoding='bytes')
            return temp
    except FileNotFoundError as e:
        raise e


def make_shape(item_str, length=2, filler='0'):
    '''returns a string with a given length. padding with 0.
    -- if item is a datetime-object, then date must be given.
    -- if item is a int or str, then date is None

    Parameters
    ----------
    item : datetime-object or str / int
        the str to be padded
    date : string
        unit (month, year, day, minute, second)
    length : int
        length of output string'''

    if not isinstance(item_str, str):
        item_str = str(item_str)

    zeros = filler
    diff = length - len(item_str)
    if diff:
        for i in range(0, diff-1):
            zeros = zeros + filler
        item_str = zeros + item_str
    return item_str


class WorkerSignals(QObject):
    '''
    Defines the signals available from a running worker thread.
    '''
    error = pyqtSignal(str)
    progress = pyqtSignal(int)
    finished = pyqtSignal(bool)

class Run_ae(QObject):
    '''worker thread for teh download'''
    def __init__(self, *args, **kwargs):
        super(Run_ae, self).__init__()
        self.signals = WorkerSignals()

    @pyqtSlot()
    def start(self):
        if DEBUG: print('Thread connected')

    @pyqtSlot(list)
    def run(self, runs):
        """Handles receiving of messages."""

        if not len(runs) > 3:
            self.signals.error.emit('%s' % 'received no arc_egmo.ste to run.')
            return
        if DEBUG: print(os.getcwd())
        self.signals.finished.emit(False)

        try:
            os.chdir(SETUP.BFOLDER_AE)
            if DEBUG: print(os.getcwd())
            for i, fls in enumerate(runs[3:]):
                if DEBUG: print(runs[1], fls)
                ### clean directories
                if DEBUG: print('deleting', os.path.join(SETUP.BFOLDER_RES, 'KOSTRA_'+fls[-17:-13]))
                try:
                    clean_folder(os.path.join(SETUP.BFOLDER_RES, 'KOSTRA_'+fls[-17:-13]))
                    clean_folder(os.path.join(SETUP.BFOLDER_GIS, 'select'))
                except Exception as e:
                    if DEBUG: print('no directory to clean %s' % e)
                remove_file_ending(SETUP.BFOLDER_KOSTRAF, 'kli')
                remove_file_ending(SETUP.BFOLDER_KOSTRAF, 'xlx')
                remove_file_starts(SETUP.BFOLDER_KOSTRAF, 'KostraWerte')
                remove_file_starts(SETUP.BFOLDER_KOSTRAF, 'Protokoll_kostra')
                ### run tables for precipitation values
                os.chdir(SETUP.BFOLDER_KOSTRAF)
                run_kostra = ['Kostra_HYMO.exe', 'kostra_sel.dbf']
                run_kostra.extend(runs[:3])
                if DEBUG: print(os.getcwd(), run_kostra)
                p = subprocess.Popen(run_kostra,
                stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)
                if DEBUG: print(p.communicate())
                ### run arcegmo
                os.chdir(SETUP.BFOLDER_AE)
                p = subprocess.Popen([os.path.join(SETUP.BFOLDER_AE, SETUP.AE_EXE), fls],
                    stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)#
                time.sleep(0.1)
                if DEBUG: print(p.communicate())
                self.signals.progress.emit(i+1)
            self.signals.finished.emit(True)
        except Exception as e:
            time.sleep(0.1)
            self.signals.error.emit('%s' % e)

    def stop(self):
        self.terminate()



class Start_kostra(QDialog, Ui_Kostra):
    '''the main class'''
    signal_fgwtool = pyqtSignal(int)
    signal_fgwtool2 = pyqtSignal(list)
    send_ae = pyqtSignal(list)
    signal_ko_re = pyqtSignal(bool)

    def __init__(self, iface=None):
        '''parent... Qt Class, userDict... dict, userDictOld... dict,
        mode... int (0 new well, 1 temporary, 2 selection)'''
        QDialog.__init__(self)#, parent.mainWindow()
        self.setupUi(self)
        self.iface = iface
        self.fgw_tool = Start_dialog_fgw_tool(iface)
        ### parameter
        self.mode = 0 ### 0 kostra, 1 rewanus
        self.do_timesteps = []
        self.do_period = []
        self.do_hq_t = []
        self.p_pmod = '0'
        self.p_ides = '0'
        self.p_recs = '0'
        self.p_imos = '0'
        self.p_conf = 'file'
        self.do_fgw = None
        self.did_run = False
        self.did_read = False
        self.runs_now = False
        self.results = {}
        self._thread = None
        self._threaded = None

        self.plot_mode = 2
        #load_layers()
        ############################
        ### lists of GUI elements for logic
        ############################

        self.chb_hq = [
            self.checkBox_hq1, self.checkBox_hq2, self.checkBox_hq5,
            self.checkBox_hq10, self.checkBox_hq20, self.checkBox_hq25,
            self.checkBox_hq30,self.checkBox_hq50, self.checkBox_hq100]

        self.labels = [
            self.label_5m, self.label_10m, self.label_15m,
            self.label_20m, self.label_30m, self.label_45m,
            self.label_60m,
            self.label_90m,
            self.label_2h, self.label_3h, self.label_4h,
            self.label_6h, self.label_9h,
            self.label_12h, self.label_18h,
            self.label_24h, self.label_48h,
            self.label_72h,
            self.label_4d, self.label_5d, self.label_6d,
            self.label_7d, self.label_8d, self.label_9d,
            self.label_10d]

        self.checkBoxs = [
            self.checkBox_5m, self.checkBox_10m, self.checkBox_15m,
            self.checkBox_20m, self.checkBox_30m, self.checkBox_45m,
            self.checkBox_60m,
            self.checkBox_90m,
            self.checkBox_2h, self.checkBox_3h, self.checkBox_4h,
            self.checkBox_6h, self.checkBox_9h,
            self.checkBox_12h, self.checkBox_18h,
            self.checkBox_24h, self.checkBox_48h,
            self.checkBox_72h,
            self.checkBox_4d, self.checkBox_5d, self.checkBox_6d,
            self.checkBox_7d, self.checkBox_8d, self.checkBox_9d,
            self.checkBox_10d]

        self.horizontalSliders = [
            self.horizontalSlider_5m, self.horizontalSlider_10m, self.horizontalSlider_15m,
            self.horizontalSlider_20m, self.horizontalSlider_30m, self.horizontalSlider_45m,
            self.horizontalSlider_60m,
            self.horizontalSlider_90m,
            self.horizontalSlider_2h, self.horizontalSlider_3h, self.horizontalSlider_4h,
            self.horizontalSlider_6h, self.horizontalSlider_9h,
            self.horizontalSlider_12h, self.horizontalSlider_18h,
            self.horizontalSlider_24h, self.horizontalSlider_48h,
            self.horizontalSlider_72h,
            self.horizontalSlider_4d, self.horizontalSlider_5d, self.horizontalSlider_6d,
            self.horizontalSlider_7d, self.horizontalSlider_8d, self.horizontalSlider_9d,
            self.horizontalSlider_10d] 

        ### disable certain gui elements if variable is lower/higher than a flag
        self.ko_re_div = [
            1, 1, 1,
            1, 1, 1,
            1,
            1,
            1, 1, 1,
            1, 1,
            0, 1,
            0, 0,
            0,
            2, 2, 2,
            2, 2, 2,
            2]

        ### disable all sliders, could be done in UI file
        for hrz in self.horizontalSliders:
            hrz.setEnabled(False)
        for lbs in self.labels:
            lbs.setEnabled(False)

        ### connect signals of checkboxes
        self.mapper_chb = QSignalMapper()
        for i, chb in enumerate(self.checkBoxs):
             self.mapper_chb.setMapping(chb, i)
             chb.stateChanged.connect(self.mapper_chb.map)
        self.mapper_chb.mapped.connect(self._cbs_t_clicked)

        ### connect signals of checkboxes
        self.mapper_hrz = QSignalMapper()
        for i, hrz in enumerate(self.horizontalSliders):
             self.mapper_hrz.setMapping(hrz, i)
             hrz.valueChanged.connect(self.mapper_hrz.map)
        self.mapper_hrz.mapped.connect(self._hrz_changed)

        ### group for precipitation pattern
        self.ButtonGroupState = QButtonGroup()
        self.ButtonGroupState.addButton(self.radioButton_ls, 0)
        self.ButtonGroupState.addButton(self.radioButton_bl, 1)
        self.ButtonGroupState.addButton(self.radioButton_rs, 2)

        ### group for plotting mode
        self.ButtonGroupState2 = QButtonGroup()
        self.ButtonGroupState2.addButton(self.radioButton_overview, 0)
        self.ButtonGroupState2.addButton(self.radioButton_detail, 1)
        self.ButtonGroupState2.addButton(self.radioButton_hq_dur, 2)

        ### group for database selection
        self.ButtonGroupState3 = QButtonGroup()
        self.ButtonGroupState3.addButton(self.radioButton_kostra, 1)
        self.ButtonGroupState3.addButton(self.radioButton_rewanus, 2)

        ### signals
        self.pushButton_plot.clicked.connect(self.click_plotting)
        self.pushButton_run_arcegmo.clicked.connect(self.click_run_ae)
        self.pushButton_fgwtool.clicked.connect(self._show_fgw_tool)
        self.pushButton_savetable.clicked.connect(self.click_showsave)
        self.pushButton_pload.clicked.connect(self.load_project)
        self.pushButton_psave.clicked.connect(self.save_project)
        self.horizontalSlider_moisture.valueChanged.connect(self._signal_hrz_changed)
        self.fgw_tool.signal_fgwtool.connect(self._signal_fgw_to_use)
        self.fgw_tool.signal_fgwtool2.connect(self._signal_overview)
        self.signal_ko_re.connect(self.fgw_tool._set_ko_re)
        self.signal_ko_re.emit(self.radioButton_kostra.isChecked())
        self.comboBox_config_file.currentIndexChanged.connect(self._signal_prevent15)
        self.ButtonGroupState2.buttonClicked[QAbstractButton].connect(self._signal_btngroup2)
        self.ButtonGroupState3.buttonClicked[QAbstractButton].connect(self._signal_btngroup3)
        self.pushButton_savetable.setEnabled(False)
        self.pushButton_plot.setEnabled(False)

        if DEBUG:
            self.did_run = True
            #self.checkBox_15m.setChecked(True)
            #self.checkBox_30m.setChecked(True)
            self.checkBox_9h.setChecked(True)
            self.checkBox_72h.setChecked(True)
            #self.checkBox_hq1.setChecked(True)
            #self.checkBox_hq5.setChecked(True)
            self.checkBox_hq30.setChecked(True)
            self.checkBox_hq100.setChecked(True)
            self.do_fgw = 3230
            self.setCB(self.comboBox_config_file, 'Kostra-Berechnung 15min')


    def _signal_prevent15(self):
        if self.comboBox_config_file.currentText()[-5:-3] == '15' \
        or self.radioButton_rewanus.isChecked():
            state = False
        else:
            state = True
        for i in range(2):
            self.checkBoxs[i].setChecked(False)
            self.checkBoxs[i].setEnabled(state)

    def _signal_btngroup2(self, btn):
        '''signal disable/enable comboboxes for plotting'''
        self.plot_mode = self.ButtonGroupState2.checkedId()
        if DEBUG: print('signal plot mode', self.plot_mode)
        if self.plot_mode == 0:
            state_dur = False
            state_hq = True
        elif self.plot_mode == 1:
            state_dur = True
            state_hq = True
        elif self.plot_mode == 2:
            state_dur = True
            state_hq = False
        self.comboBox_duration.setEnabled(state_dur)
        self.comboBox_hq.setEnabled(state_hq)


    def _signal_btngroup3(self, btn):
        '''signal for database selection / disable GUI elements'''

        mode = self.ButtonGroupState3.checkedId()
        if DEBUG: print('signal plot mode', self.mode)

        for i, krdiv in enumerate(self.ko_re_div):
            self.checkBoxs[i].setEnabled(krdiv == mode or krdiv == 0)
            if not self.checkBoxs[i].isEnabled(): self.checkBoxs[i].setChecked(False)

        self._signal_prevent15()

        tmp_state = True if mode==1 else False

        self.checkBox_hq3.setEnabled(tmp_state)
        if not self.checkBox_hq3.isEnabled(): self.checkBox_hq3.setChecked(False)

        self.checkBox_hq30.setEnabled(tmp_state)
        if not self.checkBox_hq30.isEnabled(): self.checkBox_hq30.setChecked(False)

        self.checkBox_hq25.setEnabled(not tmp_state)
        if not self.checkBox_hq25.isEnabled(): self.checkBox_hq25.setChecked(False)

        self.signal_ko_re.emit(self.radioButton_kostra.isChecked())

    def _signal_process(self, i):
        '''signal for process label updates'''
        self.label_signal.setText('Berechne...{}/{}'.format(i, len(self.do_hq_t)))
        self.label_signal.setStyleSheet('color: blue')


    def _signal_error(self, e):
        '''signal for error message'''
        self.label_signal.setText('Fehler in Berechnung! %s' % e)
        self.label_signal.setStyleSheet('color: red')


    def _signal_fin(self, check):
        '''signal on start/end of simulation run. deactive/activate GUI elements'''
        if DEBUG: print('_signal_fin', check)
        if check:
            self.label_signal.setText('Berechnung beendet')
            self.label_signal.setStyleSheet('color: green')
        ### prepare the GUI for plotting
        self.radioButton_detail.setEnabled(check)
        self.radioButton_hq_dur.setEnabled(check)
        self.pushButton_fgwtool.setEnabled(check)
        self.radioButton_overview.setEnabled(check)
        self.comboBox_duration.setEnabled(check)
        self.pushButton_savetable.setEnabled(check)
        self.pushButton_plot.setEnabled(check)
        self.did_run = check
        if check:
            self.comboBox_duration.clear()
            self.comboBox_hq.clear()
            [self.comboBox_hq.addItem('HQ'+make_shape(t, length=3)) for t in self.do_hq_t]
            [self.comboBox_duration.addItem(make_shape(t, length=4)+'min') for t in self.do_timesteps]
        self.did_read = not(check)
        if DEBUG: print('_signal_fin', self.did_read)


    def _signal_overview(self, xlist):
        '''signal to give overview about changed selections from iface'''
        if DEBUG: print('update', xlist)
        self.lineEdit__ofgw.setText(str(int(xlist[0])))
        self.lineEdit_oriver.setText(str(xlist[1]))
        self.lineEdit_oezgarea.setText(str(xlist[2]))


    def _signal_fgw_to_use(self, fgw):
        '''the FGWID to run the model for'''
        self.do_fgw = fgw


    def _show_fgw_tool(self):
        self.fgw_tool.save_as = SETUP.BFOLDER_GIS
        self.fgw_tool.show()


    def _signal_hrz_changed(self):
        '''signal method to change the color of the moisture condition label'''
        state = self.horizontalSlider_moisture.value()
        if state > 60:
            self.label_premoisture.setStyleSheet('color: blue')
        elif state < 30:
            self.label_premoisture.setStyleSheet('color: red')
        else:
            self.label_premoisture.setStyleSheet('color: black')
        self.label_premoisture.setText('Meteor. Vorfeuchte:   {:04.2f}'.format(float(state)/100))


    def _cbs_t_clicked(self, index):
        '''signal method for the checkBoxes, activate sliders and labels'''
        state = self.checkBoxs[index].isChecked()
        self.horizontalSliders[index].setEnabled(state)
        self.labels[index].setEnabled(state)
        if DEBUG: print("Checkbox at:", index, " - text:", self.checkBoxs[index].text())


    def _hrz_changed(self, index):
        '''signal method for the horizontalsliders, change label values'''
        state = self.horizontalSliders[index].value()
        self.labels[index].setText(str(state))
        if DEBUG: print("slider at:", index, " - text:", self.checkBoxs[index].text())


    def setCB(self, comboBox, text):
        '''prepare the comboBox to show a certain text'''
        index = comboBox.findText(text, Qt.MatchFixedString)
        if index >= 0:
             comboBox.setCurrentIndex(index)


    def save_project(self):
        self.get_settings()
        try:
            ### get every setting, easier to restore
            dur_states = []
            ts_states = []
            for i, chb in enumerate(self.checkBoxs):
                state = chb.isChecked()
                ts_states.append(state)
                dur_states.append(self.horizontalSliders[i].value())

            save_d = {
                'do_hqt_': self.do_hq_t,
                'periods': dur_states,
                'timesteps': ts_states,
                'do_fgw': self.do_fgw,
                'p_mod': self.p_pmod,
                'P_ides': self.p_ides,
                'p_rces': self.p_recs,
                'p_imos': self.p_imos,
                'p_conf': self.p_conf,
                'mode': self.mode,
                'fgw_name': self.lineEdit_oriver.text(),
                'fgw_area': self.lineEdit_oezgarea.text()
                }
            path_name = QFileDialog.getSaveFileName(None, 'Projekt speichern', '',
                "Kostra Projekt Datei (*.klhw)")[0]
            save_obj_compressed(path_name, save_d)
        except Exception as e:
            self.iface.messageBar().pushMessage(u'Projekt speichern fehlgeschlagen.',
              level=Qgis.Warning, duration=7)
            if DEBUG: print(e)


    def load_project(self):
        path_name = QFileDialog.getOpenFileName(None, 'Projekt öffnen', '',
            "Kostra Projekt Datei (*.klhw)")[0]
        try:
            load_d = load_obj_compressed(path_name)
            
            ### set everythind to disabled
            for hrz in self.horizontalSliders:
                hrz.setEnabled(False)
            for lbs in self.labels:
                lbs.setEnabled(False)
            for hqt in self.chb_hq:
                hqt.setChecked(False)

            ### find idx of selected HQT and enable checkBox
            for hqt in load_d['do_hqt_']:
                idx = [x.text() for x in self.chb_hq].index(hqt)
                self.chb_hq[idx].setChecked(True)

            for i, state in enumerate(load_d['timesteps']):
                self.checkBoxs[i].setChecked(state)
                self.horizontalSliders[i].setValue(float(load_d['periods'][i]))

            self.lineEdit__ofgw.setText(str(self.do_fgw))
            self.lineEdit_oriver.setText(load_d['fgw_name'])
            self.lineEdit_oezgarea.setText(load_d['fgw_area'])
            self.doubleSpinBox_init_q.setValue(float(load_d['P_ides']))
            self.doubleSpinBox_recession.setValue(float(load_d['p_rces']))
            self.horizontalSlider_moisture.setValue(float(load_d['p_imos']))

            if load_d['p_conf'] == '05':
                outfile = 'Kostra_run_5min.bat'
            else:
                outfile = 'Kostra_run_15min.bat'
            self.setCB(self.comboBox_config_file, outfile)

            if load_d['p_mod'] == 0:
                self.radioButton_rs.setChecked(True)
            elif load_d['p_mod'] == 1:
                self.radioButton_bl.setChecked(True)
            else:
                self.radioButton_ls.setChecked(True)
                
            if load_d['mode']:
                self.radioButton_rewanus.setChecked(True)
            else:
                self.radioButton_kostra.setChecked(True)

        except Exception as e:
            self.iface.messageBar().pushMessage(u'Projekt laden fehlgeschlagen.',
              level=Qgis.Warning, duration=7)
            print(e)


    def get_settings(self):
        '''read all the user defined settings from the GUI'''
        self.do_timesteps = []
        self.do_period = []
        self.do_hq_t = []
        ### evaluate settings for duration times and the respective periods
        for i, chb in enumerate(self.checkBoxs):
            if chb.isChecked():
                txt = chb.text()
                timedec = txt[txt.find(' ')+1]
                value = txt[:txt.find(' ')]
                if timedec == 'S':
                    multiplier = 60
                elif timedec == 'T':
                    multiplier = 1440
                else:
                    multiplier = 1
                self.do_timesteps.append(int(value) * multiplier)
                self.do_period.append(str(self.horizontalSliders[i].value()))
        ### evaluate the settings for return periods
        for hq in self.chb_hq:
            if hq.isChecked():
                self.do_hq_t.append(hq.text())
        ### evaluate the distribution of precipitation
        self.p_pmod = int(self.ButtonGroupState.checkedId())
        if DEBUG: print('p_mod', self.p_pmod)
        ### intitial discharge and recession parameter
        self.p_ides = str(self.doubleSpinBox_init_q.value())
        self.p_recs = str(self.doubleSpinBox_recession.value())
        ### initial moisture
        self.p_imos = str(self.horizontalSlider_moisture.value())
        ### configuration file
        #self.p_conf = self.comboBox_config_file.currentText()
        self.p_conf = self.comboBox_config_file.currentText()[-5:-3]


    def check_settings(self):
        '''some necessary checks before a model run'''
        ok = True
        if not self.do_hq_t:
            self.iface.messageBar().pushMessage(u'Noch keine Jährlichkeiten ausgewählt.',
              level=Qgis.Info, duration=3)
            if DEBUG: print(u'Noch keine Jährlichkeiten ausgewählt.')
            ok = False
        if not self.do_timesteps:
            self.iface.messageBar().pushMessage(u'Noch keine Dauerstufen ausgewählt.',
              level=Qgis.Info, duration=3)
            if DEBUG: print(u'Noch keine Dauerstufen ausgewählt.')
            ok = False
        return ok


    def create_wiederkehrsintervall_txt(self):
        '''create the Wiederkehrsintervalle.txt configuration file'''
        if DEBUG: print(self.do_hq_t)
        with open(os.path.join(SETUP.BFOLDER_KOSTRAF, 'Wiederkehrintervalle.txt'), 'w') as fid:
            fid.write('Wiederkehrintervalle[a]' + '\n')
            for rett in self.do_hq_t:
                fid.write(str(rett) + '\n')


    def create_dauerstufen_txt(self):
        '''create the Dauerstufen.txt configuration file'''
        with open(os.path.join(SETUP.BFOLDER_KOSTRAF, 'Dauerstufen.txt'), 'w') as fid:
            fid.write('Dauerstufe[min]\tHW_Dauer[d]' + '\n')
            for i, ds in enumerate(self.do_timesteps):
                fid.write(str(ds) + '\t'+ str(self.do_period[i]) + '\n')


    def create_ae_file(self):
        '''modify the arc_egmo.ste config files'''
        ae_min = self.comboBox_config_file.currentText()[-5:-3]
        for hq in self.do_hq_t:
            xfile = 'arc_egmo_k_t{}_DT{}min.tmp'.format(make_shape(hq, length=3),
                ae_min)
            with codecs.open(os.path.join(SETUP.BFOLDER_AE, xfile), 'r', encoding=DECODER) as fidr:
                afile = 'arc_egmo_k_t{}_DT{}min.ste'.format(make_shape(hq, length=3),
                    ae_min)
                with codecs.open(os.path.join(SETUP.BFOLDER_AE, afile), 'w', encoding=DECODER) as fidw:
                    for liner in fidr:
                        linetmp = liner.split()
                        if not linetmp:
                            fidw.write('\r\n')
                            continue
                        if linetmp[0] == 'PROJEKT':
                            fidw.write(linetmp[0] + '\t' + SETUP.BFOLDER + '\r\n')
                        elif linetmp[0] == 'BERECHNUNGEN_FUER':
                            fidw.write(linetmp[0] + '\t' + str(self.do_fgw) + '\t' + str(self.do_fgw) + '\r\n')
                        else:
                            fidw.write(liner)


    def create_mo_file(self):
        '''modify the module.ste config files'''
        ae_min = self.comboBox_config_file.currentText()[-5:-3]
        for hq in self.do_hq_t:
            xfile = 'modul_k_DT{}min.tmp'.format(ae_min)
            #print(os.path.join(BFOLDER_AE, xfile))
            with codecs.open(os.path.join(SETUP.BFOLDER_AE, xfile), 'r', encoding=DECODER) as fidr:
                afile = 'modul_k_DT{}min.ste'.format(ae_min)
                with codecs.open(os.path.join(SETUP.BFOLDER_AE, afile), 'w', encoding=DECODER) as fidw:
                    for liner in fidr:
                        linetmp = liner.split()
                        if not linetmp:
                            fidw.write('\r\n')
                            continue
                        if linetmp[0] == 'RUECKGANGSFAKTOR' and linetmp[1] == 'XXXX':
                            fidw.write(linetmp[0] + '\t' + str(self.p_recs) + '\r\n')
                        elif linetmp[0] == 'ANFANGSABFLUSS':
                            fidw.write(linetmp[0] + '\t' + str(self.p_ides) + '\r\n')
                        elif linetmp[0] == 'MET_VORGESCHICHTE':
                            fidw.write(linetmp[0] + '\t' + str(int(self.p_imos)/100) + '\r\n')
                        else:
                            fidw.write(liner)


    def collect_results(self):
        '''read the results - discharge and mean precipitation'''
        failed = []
        if not self.did_read:
            res_collection = {}

            for i, t in enumerate(self.do_hq_t):
                t_str = make_shape(t, length=3)
                qcf = os.path.join(SETUP.RFOLDER.format(t_str), 'fgw_mit.qc')
                pif = os.path.join(SETUP.RFOLDER.format(t_str), 'geb_mit.pi')
                if DEBUG: print(qcf, pif)
                if not os.path.isfile(qcf):
                    failed.append(['qc:', t])
                    if DEBUG: print('failed', qcf)
                else:
                    qcfiles = arcegmo_qc(qcf)
                    if str(self.do_fgw) in qcfiles:
                        res_collection.update({str(t): qcfiles[str(self.do_fgw)]})
                if not os.path.isfile(pif):
                    failed.append(['pi:', t])
                else:
                    pifiles = arcegmo_qc(pif)
                    if '0' in pifiles:
                        res_collection.update({'P' + str(t): pifiles['0']})
                if not i and os.path.isfile(qcf):
                    res_collection.update({'time': qcfiles['time']})
            if failed:
                self.iface.messageBar().pushMessage(u'Konnte nicht einlesen: {}.'.format(failed),
                    level=Qgis.Info, duration=7)
            self.results = res_collection
            self.did_read = True


    def segments_timeseries(self):
        '''calculate the segements of the individual simulation periods for the durations'''
        if DEBUG: print(self.do_hq_t, [x for x in self.results])
        if not str(self.do_hq_t[0]) in self.results:
            self.iface.messageBar().pushMessage(u'Keine Abbildung wegen Einlesefehler.',
                    level=Qgis.Info, duration=7)
            return True

        ### we start at the end and work towards the start, as we don't know the
        # setting time
        x_len = len(range(len(self.results[str(self.do_hq_t[0])])))
        self.dur_x = [x_len]
        dur_sum = x_len
        for i, dur in enumerate(self.do_period[::-1]):
            dur_sum +=  -1 * int(dur) * 1440 / int(self.p_conf) - 1
            self.dur_x.append(int(dur_sum))

        self.dur_xpos = self.dur_x[::-1]
        self.dur_xpos.insert(0, 0)
        return False


    def plot_overview_durations(self):
        '''plot all the HQTs for one duration'''
        if DEBUG: print('plot_overview_durations')
        pos = self.do_timesteps.index(int(self.c_dur))
        q_max = []
        for hq in self.do_hq_t:
            p1 = self.dur_xpos[pos+1]#+1
            p2 = self.dur_xpos[pos+2]#+2
            flow = self.results[str(hq)][p1:p2]
            q_max.append(max(flow)[0])
        fig, ax = plt.subplots(1, 1)
        ax.plot(q_max, marker='s')
        ax.set_ylabel(u'Q (m³/s)', size=14)
        ax.set_xticks(range(0, len(self.do_hq_t)))
        ax.set_xticklabels(['HQ'+str(int(x)) for x in self.do_hq_t])
        ax.set_title('Dauerstufe {} min'.format(self.c_dur), size=14)
        ax.set_xlabel('Wiederkehrintervall (T)', size=14)
        ax.grid(True)
        plt.show()


    def plot_overview_detail(self):
        '''plot a combination of duration and HQT'''
        if DEBUG: print('plot_overview_detail')
        pos = self.do_timesteps.index(self.c_dur)
        p1 = self.dur_xpos[pos+1]#+1
        p2 = self.dur_xpos[pos+2]#+2
        prec = self.results['P'+str(self.c_hq)][:, 0]
        prec[prec == 0] = np.NaN
        x = range(len(prec))
        disc = self.results[str(self.c_hq)][:, 0]
        title = 'HQ {}, Dauerstufe {}'.format(self.c_hq, self.c_dur)
        plot_detail(list(range(len(x[p1:p2]))), prec[p1:p2], disc[p1:p2], title)
        plt.show()


    def csv_overview(self):
        '''create and save the results in a CSV file'''
        ok = self.check_settings()
        if not ok:
            if DEBUG: print('!')
            return

        if not self.results:
            self.collect_results()
            chk = self.segments_timeseries()
            if chk:
                if DEBUG: print('!')
                return
        if DEBUG: print(1)
        header= 'HQ(T);' + ';'.join([str(dur)+'min' for dur in self.do_timesteps])
        table = np.zeros((len(self.do_hq_t), len(self.do_timesteps)+1))
        if DEBUG: print(2)
        for i, hq in enumerate(self.do_hq_t):
            table[i, 0] = hq
            for j, dur in enumerate(self.do_timesteps):
                pos = self.do_timesteps.index(int(dur))
                p1 = self.dur_xpos[pos+1]#+1
                p2 = self.dur_xpos[pos+2]#+2
                try:
                    flowm = max(self.results[str(hq)][p1:p2])[0]
                except:
                    flowm = -999
                    if DEBUG: print(hq, dur)
                table[i, j+1] = flowm
        if DEBUG: print(3)
        return [header, table]



    def plot_overview(self):
        '''plot the whole timeseries of precipitation and discharge'''
        if DEBUG: print('plot_overview')
        prec = self.results['P'+str(self.c_hq)][self.dur_xpos[1]:, 0]
        prec[prec == 0] = np.NaN
        disc = self.results[str(self.c_hq)][self.dur_xpos[1]:, 0]
        x = range(len(prec))
        fig, axs = plt.subplots(2, 1, sharex=True)
        fig.subplots_adjust(hspace=0)
        ax1, ax2 = axs
        ax2.set_xlabel("Dauerstufen", size=14)
        ax1.set_ylabel("P (mm/h)", size=14)
        ax2.set_ylabel(u"Q (m³/s)", size=14)
        ax1.set_title(u"HQ{}".format(self.c_hq))

        ax1.fill_between(x, 0, prec,
            linewidth=1, linestyle = '-', label="P", color='steelblue')

        ax2.plot(x, disc)
        ax2.fill_between(x, 0, disc,
                         alpha=0.5, linewidth=2, label="HQ{}".format(self.c_hq))
        ax1y = ax1.get_ylim()
        ax1.spines['bottom'].set_visible(False)
        ax2.spines['top'].set_visible(False)
        ax1.set_ylim(ax1.get_ylim()[::-1])
        ax1.yaxis.tick_right()
        ax1.yaxis.set_label_position("right")
        ax1.axhline(y=ax1y[0], linewidth=1, color='k', linestyle='--')
        ax1.set_xticks([x - self.dur_xpos[1] for x in self.dur_xpos[1:]])
        ax1.set_xticklabels([' '+str(int(x)) for x in self.do_timesteps])
        ax1.grid(True)
        ax2.grid(True)
        plt.show()


    def click_showsave(self):
        '''show the dialog to save in a folder to save the CSV file with the resuls'''
        try:
            path_name = QFileDialog.getExistingDirectory(None,
                                                         'Ausgabeverzeichnis',
                                                         SETUP.BFOLDER_AE)
            table_d = self.csv_overview()
            with open(os.path.join(path_name, '{}_HQT_Dauerstufe.csv'.format(self.do_fgw)), 'w') as fid:
                np.savetxt(fid, table_d[1], delimiter=';', header=table_d[0], fmt=('%3.2f'))
        except Exception as e:
            if DEBUG: print(e)
            self.iface.messageBar().pushMessage(u'Fehler: %s.' % e,
                                     level=Qgis.Warning, duration=7)

    def click_run_ae(self):
        '''set up the configuration files and create BAT-file.
        --> no matter how, we can't get the EXE files work properly from
        within QGIS.'''
        cdir = os.getcwd()
        if not self.lineEdit__ofgw.text():
            self.iface.messageBar().pushMessage(u'Bitte erst FGW auswählen.',
                                     level=Qgis.Info, duration=3)
            return

        if not self._thread:
            self._thread = QThread()
            if DEBUG: print('setting qthread')

        if not self._threaded:
            if DEBUG: print('run_ae init')
            self._threaded = Run_ae()
            self.send_ae.connect(self._threaded.run)
            self._threaded.signals.progress.connect(self._signal_process)
            self._threaded.signals.error.connect(self._signal_error)
            self._threaded.signals.finished.connect(self._signal_fin)
            self._thread.started.connect(self._threaded.start)
            self._threaded.moveToThread(self._thread)
            if DEBUG: print('ae_run moved to thread')
        qApp.aboutToQuit.connect(self._thread.quit)
        self._thread.start()

        self.results = {}

        try:
            self.did_run = False
            self.get_settings()
            ok = self.check_settings()
            if not ok:
                if DEBUG: print('bad settings')
                return

            if DEBUG: print(1)
            ### create/edit configuration files
            self.create_wiederkehrsintervall_txt()
            self.create_dauerstufen_txt()
            self.create_ae_file()
            self.create_mo_file()
            if DEBUG: print(2)
            ### create precipitation patterns

            #run_meteo = 'kostra_sel.dbf 0 {} {}'.format(self.p_conf, self.p_pmod)
            if DEBUG: print(['Kostra_HYMO.exe', 'kostra_sel.dbf', '0',
                str(self.p_conf), str(self.p_pmod)])
            self.label_signal.setText('Bereite Niederschlag vor...')
            runfiles = [
                '0',
                str(self.p_conf),
                str(self.p_pmod)]

            for i, hq in enumerate(self.do_hq_t):
                runf = 'arc_egmo_k_t{}_DT{}min.ste\n'.format(make_shape(hq, length=3),
                          self.p_conf)
                runfiles.append(os.path.join(SETUP.BFOLDER_AE, runf))

            ### signal the Worker to start ArcEGMO
            self.send_ae.emit(runfiles)

        except Exception as e:
            if DEBUG:
                print('Fehler beim erstellen der BAT-Dateien: {}'.format(e))
                self.iface.messageBar().pushMessage(u'Fehler: %s.' % e,
                                     level=Qgis.Info, duration=3)
        finally:
            os.chdir(cdir)


    def click_plotting(self):
        '''plot one of the three plot options'''
        ### initialize the Worker and move it to the QTread
        self.get_settings()
        self.c_dur = int(self.comboBox_duration.currentText()[:4])
        self.c_hq = int(self.comboBox_hq.currentText()[2:])
        ok = self.check_settings()

        if not ok:
            return

        if not self.did_run:
            self.iface.messageBar().pushMessage(u'Noch keine Modellrechnungen durchgeführt.',
              level=Qgis.Info, duration=3)
            return

        self.collect_results()
        chk = self.segments_timeseries()
        if chk:
            return

        if self.plot_mode == 0:
            self.plot_overview()
        elif self.plot_mode == 1:
            self.plot_overview_detail()
        elif self.plot_mode == 2:
            self.plot_overview_durations()

    def closePlugin(self):
        try:
            self._thread.quit()
        except Exception as e:
            if DEBUG: print(e)
        self.close()


    def closeEvent(self, event):
        try:
            self._thread.quit()
        except Exception as e:
            if DEBUG: print(e)
        self.fgw_tool.closePlugin()
        event.accept()

if __name__ == "__main__":
    print('this is a Qgis plugin ')