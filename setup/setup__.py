# -*- coding: utf-8 -*-
"""
Created on Mon May 27 16:32:47 2019

@author: ruben.mueller
"""

import os

# name der ausführbaren Date von ArcEGMO
# name of the ArEGMO executable
AE_EXE = 'ae_5.exe'

# Projektverzeichnis des ArcEGMO-Modells
# root folder of the ArcEGMO model
BFOLDER = 'E:\\Modell\\NA_Modell-KOSTRA'

# Name des TG-Layer
# name of the TG layer (sub-catchments)
TG_LAYER = 'tg'

# Name des KOSTRA/Rewanus-Layer
# name of the KOSTRA/Rewanus layer
KOSTRA_LAYER = 'kostra_st'
REWANUS_LAYER = 'rewanus_st'

# Name des Fließgewässerabschnitt-Layer
# name of the layer with the flow arcs
FGW_LAYER = 'fgw'

# FGWID
FGW_ID = 'FGWID'
# FGWULID
FGWUL_ID = 'FGWULID'
# TGID in FGW
FGWEZG = 'TGID'
# TGID in TG
TGEZG = 'TGID'

# Area behind FGW
FAREA = 'K_AREA'
RIVER = 'GEWAESSER'

####
# nicht ändern / no not edit
####

DEBUG = True # False # 

BFOLDER_KOSTRAF = os.path.join(BFOLDER, 'Zeit.Dat', 'met_data', 'KOSTRA')
BFOLDER_RES = os.path.join(BFOLDER, 'results')
BFOLDER_AE =  os.path.join(BFOLDER, 'Arc_EGMO')
BFOLDER_GIS =  os.path.join(BFOLDER, 'Gis')
RFOLDER = os.path.join(BFOLDER, 'results', 'KOSTRA_t{}', 'zeit.dat', 'Minute')