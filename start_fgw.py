# -*- coding: utf-8 -*-
# ***************************************************************************
# start_fgw.py  -       Part of KOSTRA-Tools, Design Floods with KOSTRA/Rewanus and ArcEGMO for QGIS
#                       Developed for and funded by LHW Saxony-Anhalt
# ---------------------
#     begin                : 24.06.2019
#     copyright            : (C) 2019 by Ruben Müller, Büro für Angewandte Hydrologie, Berlin
#     email                : info at ruben.mueller at bah-berlin dot de
#
# ***************************************************************************
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU General Public License as published by  *
# *   the Free Software Foundation; either version 2 of the License, or     *
# *   (at your option) any later version.                                   *
# *                                                                         *
# ***************************************************************************
import os
import processing
from qgis.core import Qgis
from qgis.core import QgsProject
from qgis.core import QgsVectorLayer
from qgis.core import QgsVectorFileWriter
from qgis.core import QgsFeatureRequest
from qgis.core.additions.edit import edit
from qgis.utils import iface
from qgis.PyQt.QtWidgets import QDialog
from qgis.PyQt.QtCore import pyqtSignal

from .pyqtDialogs.dialog_fgw_tool import Ui_Dialog
from .setup import setup__ as SETUP


DEBUG = SETUP.DEBUG
_ver = Qgis.QGIS_VERSION_INT


def check_layer_loaded(layer_name):
    '''check if layer with layer_name is loaded

    Parameters
    ----------
    layer_name : str
        the name of the layer

    Returns
    -------
    tmp : bool
        True if layer is registered''',
    boolean = False
    layers = QgsProject.instance().mapLayers()
    for name, layer in layers.items():
        if layer.name() == (layer_name):
            boolean = True
            break
    return boolean


def load_layer(layer_name, set_active=False, folderx=None, altName=None):
    '''load an shape layer_name

    Parameters
    ----------
    layer_name : str
        the name of the layer
    set_active : bool, optional
        and set the loaded layer active if True
    folderx : str, optional
        provide an alternative path to the ESRI layer file
    altName : str, optional
        provide an alternative layer name when loaded

    Returns
    -------
    layer : layer object
        the loaded layer'''

    if folderx is None:
        folderx = '.'
    if altName is None:
        altName = layer_name
    try:
        if not check_layer_loaded(layer_name):
            layer = QgsVectorLayer(os.path.join(folderx, layer_name + ".shp"), altName, "ogr")
            QgsProject.instance().addMapLayer(layer)
        else:
            layer = QgsProject.instance().mapLayersByName(layer_name)[0]
        if set_active:
            iface.setActiveLayer(layer)
        return layer
    except:
        iface.messageBar().pushMessage('Konnte Layer {} nicht laden.'.format(layer_name),
                                        level=Qgis.Warning, duration=7)
        return None


def getLayerHandle(layer_name,  set_active=False, folderx=None, altName=None):
    '''main file to load an shape layer_name. Look if layer is loaded and
    return the layer object or load the layer otherwise.

    Parameters
    ----------
    layer_name : str
        the name of the layer
    set_active : bool, optional
        and set the loaded layer active if True
    folderx : str, optional
        provide an alternative path to the ESRI layer file
    altName : str, optional
        provide an alternative layer name when loaded

    Returns
    -------
    layer : layer object
        the loaded layer'''

    if not check_layer_loaded(layer_name):
        return load_layer(layer_name, set_active=False, folderx=None, altName=None)
    else:
        return QgsProject.instance().mapLayersByName(layer_name)[0]


def sublayer(layer, incList, attributeName, name, ltype='MultiLine', showLayer=True):
    '''create an new layer with an subset of features from another layer

    Parameters
    ----------
    layer : layer object
        layer with the superset of features
    incList : list
        list if TGIDs or other (that may be in attributeName)
    attributeName : str
        name of the field that is compared against incList
    name : str
        name of the new layer
    showLayer : bool, optional
        show the layer on the map if True

    Returns
    -------
    memlayer : layer object
        the new layer with the subset on features'''

    feats = [feat for feat in layer.getFeatures()]
    name = name + '_new'
    ### check out geometry type of layer
    mem_layer = QgsVectorLayer("{}?crs={}".format(ltype, layer.crs().authid()), name, "memory")


    mem_layer_data = mem_layer.dataProvider()
    if DEBUG: print('sublayer', layer.name())
    attr = layer.dataProvider().fields().toList()
    mem_layer_data.addAttributes(attr)
    mem_layer.updateFields()
    mem_layer_data.addFeatures(feats)

    feats = mem_layer.getFeatures()
    tgid = mem_layer.fields().lookupField(attributeName)
    with edit(mem_layer):
        for i, feat in enumerate(feats):
                if not float(feat.attributes()[tgid]) in incList:
                    mem_layer.deleteFeature(feat.id())
    if showLayer:
        mem_layer.updateExtents()
        QgsProject.instance().addMapLayer(mem_layer)

    return mem_layer


class Start_dialog_fgw_tool(QDialog, Ui_Dialog):
    '''class for the main dialog of the stand alone water balance dialog'''

    signal_fgwtool = pyqtSignal(int)
    signal_fgwtool2 = pyqtSignal(list)

    def __init__(self, iface):
        QDialog.__init__(self)#super().__init__()
        self.setupUi(self)
        self.iface = iface
        self.fgw_ul = None
        self.fgw = None
        self.ezgfgw = None
        self.ezgfgw = None
        self.fgw_layer = None
        self.ezg_layer = None
        self.ko_re_layer = None
        self.ko_re = True
        self.idx_fgw = None
        self.idx_fgw_ul = None
        self.idx_ezgfgw = None
        self.idx_ezgezg = None
        self.selection = -999
        self.curr_fgw0 = None
        self.feat_list = []
        self.subint_fgw = []
        self.save_as = './kostra_sel.shp'
        self.ok = False
        self.subfeat_fgw = None
                
        self.pushButton_memlayer.clicked.connect(self.run)
        self.iface.mapCanvas().selectionChanged.connect(self.signal_layer)
        

    def closePlugin(self):
        self.close()


    def closeEvent(self, event):
       event.accept()
      
        
    def _set_ko_re(self, sgnl):
        self.label_2.setText(u'für KOSTRA' if sgnl else 'für REWANUS')
        self.ko_re_sel = SETUP.KOSTRA_LAYER if sgnl else SETUP.REWANUS_LAYER
        print('selection from', self.ko_re_sel )   
        
        
    def signal_layer(self):
        """signal from iface about changes selections"""
        if not self.isVisible():
           return
        area = 0
        if self.fgw_layer is None:
            self.fgw_layer = load_layer(SETUP.FGW_LAYER)
            if self.fgw_layer is None:
                iface.messageBar().pushMessage(u"FGW-Layer {} fehlt?.".format(SETUP.FGW_LAYER),
                      level=Qgis.Info, duration=3)
                return

        try:
            sfeatures = self.fgw_layer.selectedFeatures()
            if len(sfeatures) > 2:
                iface.messageBar().pushMessage(u"{} FGW ausgewählt! Nur eine Selektion zulässig".format(len(sfeatures)),
                      level=Qgis.Info, duration=3)
                return
            else:
                id_tg = self.fgw_layer.fields().lookupField(SETUP.FGW_ID)
                id_area = self.fgw_layer.fields().lookupField(SETUP.FAREA)
                id_river = self.fgw_layer.fields().lookupField(SETUP.RIVER)
                if id_tg == -1:
                    iface.messageBar().pushMessage(u"FGW-Layer enthält kein Feld {}".format(SETUP.FGW_ID),
                      level=Qgis.Info, duration=3)
                else:
                    tgid = ''
                    for feat in sfeatures:
                        tgid = feat.attributes()[id_tg]
                        river = feat.attributes()[id_river]
                        area = feat.attributes()[id_area]
                        break
                    if float(area) > 50 or float(area) < 10:
                        iface.messageBar().pushMessage(u"Achtung {} EZG mit {} km² außhalb des Bereichs 10 -50 km²".format(tgid, area),
                            level=Qgis.Info, duration=2)
                    self.lineEdit_selection.setText(str(int(tgid)))
                    self.signal_fgwtool2.emit([tgid, river, area])
                    if DEBUG: print('signal_layer', [tgid, river, area, SETUP.FAREA, id_area])
                    self.curr_fgw0 = tgid
                    
        except Exception as e:
            iface.messageBar().pushMessage(u"Fehler bei selektion mit {}".format(e),
                      level=Qgis.Info, duration=3)


    def check_layer_field(self, layer, field):
        """check if a fiels is existing in a layer"""
        if not field:
            return False
        idx = layer.fields().lookupField(field)
        return True if idx > -1 else False


    def prepare_filtering(self):
        """start the selection process for upstream FGW"""
        check = True
        if DEBUG: print('-- prepare filtering', SETUP.FGW_ID, SETUP.FGWUL_ID, SETUP.TGEZG, SETUP.FGWEZG)
        
        if not self.fgw_layer:
            self.fgw_layer = load_layer(SETUP.FGW_LAYER)
        if not self.check_layer_field(self.fgw_layer, SETUP.FGW_ID):
            iface.messageBar().pushMessage(u"Layer {} ohne benötigte Felder {}. Bitte Feldauswahl aktualisieren.".format(SETUP.FGW_LAYER,
                                           SETUP.FGW_ID),
                  level=Qgis.Info, duration=3)
            check = False
        if not self.check_layer_field(self.fgw_layer, SETUP.FGWEZG):
            iface.messageBar().pushMessage(u"Layer {} ohne benötigte Felder {}. Bitte Feldauswahl aktualisieren.".format(SETUP.FGW_LAYER, 
                            SETUP.FGWEZG),
                  level=Qgis.Info, duration=3)
            check = False
            
        if not self.ezg_layer:
            self.ezg_layer = load_layer(SETUP.TG_LAYER)
            
        if not self.check_layer_field(self.ezg_layer, SETUP.TGEZG):
            iface.messageBar().pushMessage(u"Layer {} ohne Feld {}. Bitte Feldauswahl aktualisieren.".format(SETUP.TG_LAYER,
                  SETUP.TGEZG),
                  level=Qgis.Info, duration=3)
            check = False
            

        self.ko_re_layer = load_layer(self.ko_re_sel)
        if DEBUG: print('prepare_filtering', [self.ko_re_layer.name(), self.ko_re_sel])
        if not self.ko_re_layer:
            check=False
            iface.messageBar().pushMessage(u"Konnte P-Layer {} nicht laden!".format(self.ko_er_sel),
                  level=Qgis.Info, duration=3)
         
        if not self.lineEdit_selection.text():
            self.signal_layer()
            
        self.idx_fgw = self.fgw_layer.fields().lookupField(SETUP.FGW_ID)
        self.idx_fg_ul = self.fgw_layer.fields().lookupField(SETUP.FGWUL_ID)
        self.idx_ezgfgw = self.fgw_layer.fields().lookupField(SETUP.FGWEZG)
        self.idx_ezgezg = self.ezg_layer.fields().lookupField(SETUP.TGEZG)
        if DEBUG: print('-- prepare filtering', self.idx_fgw, self.idx_fg_ul, self.idx_ezgfgw, self.idx_ezgezg)
        return check


    def filter_fgw(self):
        """the interesting part, select all upstream FGWIDs and highlight them by selection in map"""
        upstreams = []
        upstreams_feat = []
        ups_tmp = []
        iterx = 0

        if DEBUG: print('filter_fgw1', self.curr_fgw0)
        
        ### if the selection of a FGW is not done zet, retry now
        if not self.curr_fgw0:
            try:
                sfeatures = self.iface.activeLayer().selectedFeatures()
                for feat in sfeatures:
                    tgid = feat.attributes()[SETUP.FGW_ID]
                    break
                self.lineEdit_selection.setText(str(tgid))
                if DEBUG: print('signal_layer', tgid)
                self.curr_fgw0 = tgid
            except:
                iface.messageBar().pushMessage(u"Keine Auswahl getroffen.",
                      level=Qgis.Info, duration=3)
                return
            
        expr = '"{}"={}'.format(SETUP.FGW_ID, self.curr_fgw0)
        request = QgsFeatureRequest().setFilterExpression(expr)
        feat_id = self.fgw_layer.getFeatures(request)
        for feat in feat_id:
            ups_tmp.append(feat.attributes()[self.idx_fgw])
        upstreams.append(self.curr_fgw0)
        upstreams_feat.append(feat)
        
        while ups_tmp and iterx < 100:
            for curr_fgw in ups_tmp:
                expr = '"{}"={}'.format(SETUP.FGWUL_ID, curr_fgw)
                if DEBUG: print(expr)
                request = QgsFeatureRequest().setFilterExpression(expr)
                feat_id = self.fgw_layer.getFeatures(request)
                for feat in feat_id:
                    upstreams.append(feat.attributes()[self.idx_fgw])
                    upstreams_feat.append(feat)
                    ups_tmp.append(feat.attributes()[self.idx_fgw])
                del_idx = ups_tmp.index(curr_fgw)
                upstreams.append(ups_tmp.pop(del_idx))
                iterx += 1
        
        self.subint_fgw = upstreams
        self.subfeat_fgw = upstreams_feat


    def sublayer_ezg(self):
        ############################
        ### select EZGs over FGWs and create dissolved EZG layer
        ############################
        selected_ezg = []
        for feat in self.subfeat_fgw:
            ezg = feat.attributes()[self.idx_ezgfgw]
            selected_ezg.append(ezg)
        mem_layer = sublayer(self.ezg_layer, selected_ezg, 
                             SETUP.TGEZG, str(SETUP.FGWEZG)+'_TG', 
                             ltype='Polygon', showLayer=False)
        QgsProject.instance().addMapLayer(mem_layer)
        try:
            if _ver > 30600:
                self.catchment = processing.run("qgis:dissolve", {'FIELD' : [],
                    'INPUT' : mem_layer,
                    'OUTPUT' : 'TEMPORARY_OUTPUT'})['OUTPUT']
            else:
                if DEBUG: print('using buffer @ 0')
                self.catchment = processing.run("native:buffer", {
                    'INPUT': mem_layer,
                    'DISTANCE': 0,
                    'SEGMENTS': 5,
                    'END_CAP_STYLE': 0,
                    'JOIN_STYLE': 0,
                    'MITER_LIMIT': 2,
                    'DISSOLVE': True,
                    'OUTPUT': 'memory:'
                })['OUTPUT']
            print(1)
            #print(self.catchment)
            #print(2)
            if _ver < 30600:
                QgsProject.instance().addMapLayer(self.catchment)
                print(3)
                ko = load_layer(self.ko_re_sel)
                co = QgsProject.instance().mapLayersByName('TGID_TG_new')[0]
                clipx = processing.run("native:clip", { 'INPUT': ko, 
                'OUTPUT' : 'memory:', 
                'OVERLAY' : co})
                print(clipx)
                QgsProject.instance().addMapLayer(clipx['OUTPUT'])
                self.cutout = clipx
        except:
            return 1
        
        
    def sublayer_regnie(self):
        #############################
        ### clip REGNIE gridpoints over the EZG
        #############################
        try:
            ko = load_layer(self.ko_re_sel)
            co = QgsProject.instance().mapLayersByName('TGID_TG_new')[0] # load_layer('TGID_TG_new')
            print(ko, co)

            cutout = processing.runAndLoadResults("native:clip", { 'INPUT' : ko,
                'OUTPUT' : 'memory', 'OVERLAY' : co})
            print(cutout)
            QgsProject.instance().addMapLayer(cutout['OUTPUT'])
            self.cutout = cutout
        except Exception as e:
            print(e)
            return 1


    def run(self):
        check = self.prepare_filtering()
        ### no selection done -> try to read old selection else 
        if not self.lineEdit_selection.text():
            self.signal_layer()
        if not self.lineEdit_selection.text():
            iface.messageBar().pushMessage(u"Bitte erst Selektion vornehmen, dann erneut Berechnen klicken",
                      level=Qgis.Info, duration=3)
        if not check:
            iface.messageBar().pushMessage(u"FGW Liste nach Berechnung leer. Fehler.",
                      level=Qgis.Info, duration=3)
            return
        
        self.filter_fgw()
        
        if not self.subfeat_fgw:
            iface.messageBar().pushMessage(u"Keine Fließabschnitte auswählbar",
                      level=Qgis.Info, duration=3)
            return
        
        if self.sublayer_ezg():
            iface.messageBar().pushMessage(u"Fehler beim Dissolve des TG-Shapes",
                      level=Qgis.Info, duration=3)
            return
        
        if _ver > 30600:
            if self.sublayer_regnie():
                iface.messageBar().pushMessage(u"Fehler beim Ausschneieden des Kostra/Rewanus-Shapes",
                          level=Qgis.Info, duration=3)
                
                return
        self.signal_fgwtool.emit(self.curr_fgw0)
        
        if 1:
            QgsVectorFileWriter.writeAsVectorFormat(self.cutout['OUTPUT'], 
                                                    os.path.join(self.save_as, 'kostra_sel.shp'),
                                                    "utf-8", driverName="ESRI Shapefile")
            with open(os.path.join(self.save_as, 'select', 'fgw.sel'), 'w') as fid:
                fid.write('ID\n')
                fid.write(str(int(self.curr_fgw0)))
        self.close()