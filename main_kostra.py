# -*- coding: utf-8 -*-
# ***************************************************************************
# main_kostra.py  -     Part of KOSTRA-Tools, Design Floods with KOSTRA/Rewanus and ArcEGMO for QGIS
#                       Developed for and funded by LHW Saxony-Anhalt
# ---------------------
#     begin                : 24.06.2019
#     copyright            : (C) 2019 by Ruben Müller, Büro für Angewandte Hydrologie, Berlin
#     email                : info at ruben.mueller at bah-berlin dot de
#
# ***************************************************************************
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU General Public License as published by  *
# *   the Free Software Foundation; either version 2 of the License, or     *
# *   (at your option) any later version.                                   *
# *                                                                         *
# ***************************************************************************

import os

from qgis.PyQt.QtWidgets import QAction
from qgis.PyQt.QtGui import QIcon
from .start_kostra import Start_kostra
from .start_kostra import load_layers
pluginPath = os.path.dirname(__file__)


class Main:
    '''class for the plugin'''
    def __init__(self, iface):
        super(Main, self).__init__()
        ### attributes
        self.iface = iface
        self.maske0 = Start_kostra(iface)

    def initGui(self):
        self.actionK = QAction(QIcon(os.path.join(pluginPath, 'icons', 'bah_kostra.png')), "KOSTRA Tools", self.iface.mainWindow())
        self.actionK.setWhatsThis("KOSTRA Tool plugin")
        self.actionK.setStatusTip("KOSTRA Tool plugin")
        self.actionK.triggered.connect(self.maske0.show)
        self.iface.addToolBarIcon(self.actionK)
        self.iface.addPluginToMenu("&BAH", self.actionK)
        
        self.actionKm = QAction(QIcon(os.path.join(pluginPath, 'icons', 'map.png')), "KOSTRA Maps", self.iface.mainWindow())
        self.actionKm.setWhatsThis("Lade KOSTRA Maps")
        self.actionKm.setStatusTip("Lade KOSTRA Maps")
        self.actionKm.triggered.connect(load_layers)    
        self.iface.addPluginToMenu("&BAH", self.actionKm)

    def unload(self):
        self.iface.removePluginMenu("&BAH", self.actionKm)
        self.iface.removePluginMenu("&BAH", self.actionK)
        self.iface.removeToolBarIcon(self.actionK)

