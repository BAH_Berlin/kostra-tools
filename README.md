
This QGIS plugin is for modelling different design floods with the ArcEGMO model of the federal state of Saxony-Anhalt
and the DWD KOSTRA and Rewanus data bases.  
The plug-in was created on behalf on the Landesbetrieb für Hochwasserschutz und Wasserwirtschaft Saxony-Anhalt (LHW Sachsen-Anhalt).

The software needs the precipitation-runoff model ArcEGMO and suitable shape files with the KOSTRA and Rewanus data sets
to be fully functional.  
For the configuration of the plugin, use the setup__.py file in the SETUP folder.

This Software is designed for **QGIS 3.6**.


**Author**: Ruben Müller  
**E-Mail**: ruben.mueller@bah-berlin.de  
**Company**: Büro für Angewandte Hydrologie   
Inh. Dr. Bernd Pfützner   
Köberlesteig 6  
D-13156 BERLIN  

**This Software and Documentation is provided under the GPL3.**


# Handbuch

## Allgemeines

Die Vorliegende Dokumentation beschreibt ein QGIS-Plugin für die Berechnung von Bemessungshochwasser aus Bemessungsniederschlägen nach KOSTRA 2010R [DWD Climate Data Center, 2010R] und Rewanus [DWD REWANUS].

## Anwendungsbeschreibung

Der Zugriff auf die Erweiterung in QGIS erfolgt nach _Abbildung 1_ über das Menü Kostra-Tool.

![Abbildung 1](docs/source/_static/abb1.png)  
_Abbildung 1: Zugriff auf die KOSTRA-Tools in QGIS_

Das Untermenü bietet die beiden Einträge "_Kartengrundlagen laden_" und "_Berechnung durchführen_".

 
### Kartengrundlagen laden

Eine Auswahl des Eintrags lädt die folgenden drei Kartenansichten, welche für die Verwendung des Kostra-Tools benötigt werden:

    * FGW-Layer mit den Fließgewässernetz (fgw.shp)
    * TG-Layer mit den Teileinzugsgebieten (tg.shp)
    * KOSTRA-Layer mit den Niederschlagsdaten (kostra\_st.shp)
    * REWANUS-Layer mit den Niederschlagsdaten (rewanus\_st.shp)

Diese vier Layer können auch von Hand eingeladen werden, müssen aber in jedem Fall vorhanden sein.

### Kartenmaterial von OpenStreetMaps laden

Hierzu wird das Plugin _QuickMapServices_ verwendet. Im Menu unter _Web_ findet sich der Eintrag _QuickMapServices_ mit dem Untermenu _OSM_ und _OSM Standard_.


### Berechnung durchführen

Durch den Klick auf die Option _"Berechnung durchführen"_ öffnet sich der Dialog in _Abbildung 2_. Dieser bietet als Erstes Optionen zur Auswahl zwischen den beiden möglichen Modi für die Berechnungs-Zeitschritte von 5 Minuten und von 15 Minuten sowie zwischen den Niederschlags-Datengrundlagen sowie die Auswahl zwischen KOSTRA und Rewanus-Daten

Der Dialog bietet im Reiter drei Reiterfenster:

**Bemessungsniederschlag**  
>In diesem Reiterfenster lässt sich festlegen, für welche Jährlichkeiten, Dauerstufen (Zeitdauer des Niederschlagsereignisses) über welchen Simulationszeitraum ("Dauern") zu berechnen sind. Die ersten beiden ergeben gemeinsam die Niederschlagsintensität, deren Verteilung im nächsten Reiter festgelegt werden kann.

**Weitere Einstellungen**  
>Hier erfolgt die Festlegung der Art Niederschlagsverteilung und der Beschreibung der Gebietseigenschaften.

**Berechnung und Visualisierung**  
>Dieses Reiterfenster beherbergt die Funktionen zur Auswahl des Fließgewässerabschnitts, für den die Berechnung erfolgen soll, zur Berechnung der Bemessungshochwasser und zur Visualisierung der Ergebnisse.


Im Folgenden wird der Ablauf zur Berechnung und Visualisierung von Bemessungshochwasser Schritt für Schritt beschrieben.

## Handhabung

### Reiterfenster Bemessungsniederschlag

Unter "_Jährlichkeit HQ_" stehen Jährlichkeiten von einem Jahr bis hundert Jahre zur Auswahl. Für jede ausgewählte Jährlichkeit wird jede ausgewählte Dauerstufe über den zugehörigen Simulationszeitraum berechnet.

Zur Auswahl einer Dauerstufe ist das Häckchen zu setzen und ggf. der Simulationszeitraum über den Schieberegler anzupassen (1 bis 30 Tage). Dabei ist zu beachten, dass für große Niederschlagsereignisse der Simulationszeitraum ausreichend gewählt werden muss, da sonst nicht die gesamte Hochwasserwelle abgebildet wird.

Je nach getroffener Auswahl, KOSTRA oder Rewanus, stehen nur bestimmte Dauerstufen zur Verfügung. Nicht kompatible Dauerstufen sind ausgegraut.

![Abbildung 2](docs/source/_static/abb2.png)  
_Abbildung 2: Reiterfenster Bemessungsniederschlag_


### Reiterfenster Weitere Einstellungen

Die zeitliche Verteilung der Niederschlagsintensitäten über die Dauerstufe ist über die Niederschlagsverteilung anzupassen. Standardmäßig ist eine rechtsschiefe Verteilung ausgewählt, eine linksschiefe Verteilung und ein gleichmäßiger Blockregen stehen ebenfalls zur Auswahl bereit.

![Abbildung 3](docs/source/_static/abb3.png)  
_Abbildung 3: Der Dialog Nutzerverwaltung_

Die Gebietseigenschaften des Einzugsgebiets zum gewählten Fließgewässerabschnitt sind über drei Parameter konfigurierbar. Der Anfangsabfluss gibt den zum Beginn der Simulation aus dem Einzugsgebiet generierten Durchfluss. Über die meteorologische Vorfeuchte ist die Gebietsfeuchte von eher trockenen zu eher feuchten Verhältnissen adaptierbar. Der Rückgangsfaktor beeinflusst die Geschwindigkeit der Abflusskonzentration.

Die Einstellungen gelten für alle Berechnungen unter den jeweils gewählten Jährlichkeiten und Dauerstufen.

Im unteren Teil des Reiters kann das Projekt gespeichert, oder ein bereits gespeichertes Projekt neu geladen werden.

>Nur in Ordner mit ausreichenden Zugriffsrechten speichern!

### Das Reiterfenster Berechnung und Visualisierung

Im Reiter „Berechnung und Visualisierung (Abbildung 4) öffnet sich mit einem Klick auf die Schaltfläche "_Einzugsgebiet anhand FGW auswählen_" das Fenster "_Fließgewässerabschnitt auswählen_", siehe Abbildung 5. Hier werden das dem ausgewählten Fließgewässerabschnitt zugehörige Einzugsgebiet, sowie die KOSTRA- und Rewanus-Gitterpunkte bestimmt. Bitte beachten Sie, dass dieser Schritt notwendig ist.

![Abbildung 4](docs/source/_static/abb4.png)  
_Abbildung 4: Koordinaten durch Mausklick in die Karte abgreifen_

Die Auswahl eines Fließgewässerabschnitts geschieht über das Selektionswerkzeug von QGIS. Wenn das Fenster geöffnet ist, und eine Selektion getroffen wird, erscheint die zugehörige FGWID im obersten Feld des Fensters. Wurde bereits eine gültige Auswahl getroffen, das Fenster jedoch noch nicht geöffnet, dann erscheint die Anzeige der FGWID nicht, es kann jedoch trotzdem über die Schaltfläche "Berechnen" die Auswahl des dazugehörigen Einzugsgebiets und der dazugehörigen KOSTRA- und Rewanus-Gitterpunkte begonnen werden.

![Abbildung 5](docs/source/_static/abb5.png)  
_Abbildung 5: Das Fenster "Auswahl Fließgewässerabschnitt"_

Mit dem Abschluss der Berechnung erscheinen neue Layer in der Kartenansicht von QGIS. Gleichzeitig wird die Shape-Datei KOSTRA\_Sel.shp im GIS-Verzeichnis von ArcEGMO angepasst.

Mit dem Abschluss der Berechnung zeigt das Reiterfenster in "_Übersicht der derzeitigen Auswahl_" weitere Informationen zum gewählten Fließgewässerabschnitt.

Mit der Auswahl eines Fließgewässerabschnitts sind alle benötigten Angaben bereitgestellt und die Berechnung der Bemessungshochwasser kann über die Schaltfläche "Berechnung durchführen" gestartet werden. Den aktuellen Fortschritt in der Berechnung aller Jährlichkeiten zeigt ein Schriftzug unterhalb der Schaltfläche an.

Mit beendeter Berechnung sind die Einstellmöglichkeiten zur Visualisierung der Ergebnisse freigegeben. Es stehen drei Visualisierungsarten zur Auswahl bereit.

### Gesamtansicht

![Abbildung 6](docs/source/_static/abb6.png)  
_Abbildung 6: Visualisierung "Gesamtansicht"

Die Gesamtansicht zeigt für eine zuvor im Reiterfenster ausgewählte Jährlichkeit die für unterschiedliche Dauerstufen generierten Hochwasserwellen, siehe Abbildung 6. Hierbei werden die jeweils ausgewählten Dauerstufen in Minuten auf der x-Achse über den jeweils gewählten Simulationszeitraum dargestellt. Ausschnitt HQ-Dauerstufe

![Abbildung 7](docs/source/_static/abb7.png)  
_Abbildung 7: Visualisierung Ausschnitt HQ-Dauerstufe_

Mit dieser Visualisierung kann eine einzelne Hochwasserwelle angezeigt werden. Im Reiterfenster sind dazu eine Jährlichkeit und Dauerstufe auszuwählen.

### Überblick Dauerstufe

Diese Visualisierung zeigt für eine zuvor im Reiterfenster ausgewählte Dauerstufe die Scheiteldurchflüsse aus den Hochwasserwellen der berechneten Jährlichkeiten.

![Abbildung 8](docs/source/_static/abb8.png)  
_Abbildung 8: Visualisierung nach der Auswahl "Überblick Dauerstufe"_

### Menüleiste in den Visualisierungsfenstern

Die Fenster bieten in der Menüleiste eine Reihe an Funktionalitäten, die Bordmittel des zur Visualisierung genutzten Pythonmoduls _matplotlib_ sind. Auf den Funktionsumfang und die Art des Fensteraufbaus kann daher kein Einfluss genommen werden.

Da die Funktionalitäten zur nutzerseitigen Modifikation der Abbildungen nützlich sein können, sollen diese hier in kürze erläutert werden.

**Das Schieberegler-Symbol)** ![](docs/source/_static/abb14.png)

Hinter dem Schieberegler-Symbol verbirgt sich ein Fenster zum Anpassen der Seitenränder der Abbildung, siehe Abbildung 9. Unter _Borders_ (Seitenränder) lassen sich die Abstände nach oben (_top_), unten (_bottom_), links (_left_) und rechts (_right_) feineinstellen.

Besitzt die Abbildung mehrere Unterabbildungen, dann können die horizontalen (_hspace_) und vertikalen (_wspace_) Abstände zwischen diesen angepasst werden.

Die Schaltfläche "_Tight layout_" (Enges layout) versucht den verfügbaren Platz bestmöglich auszunutzen, "_Reset_"; stellt die vorgenommenen Änderungen zurück.

![Abbildung 9](docs/source/_static/abb9.png)  
_Abbildung 9: Fenster zum Anpassen der Seitenränder_

**Das Diagramm-Symbol mit dem Pfeil** ![](docs/source/_static/abb15.png)

Unter dem Reiter Axes lassen sich zunächst die Achsen auswählen, wenn mehr als eine Unterabbildung existiert. Wenn nicht, erscheint gleich das Fenster wie in Abbildung 10.

![Abbildung 10](docs/source/_static/abb10.png)  
_Abbildung 10: Fenster zum Anpassen der Achsen und Liniendarstellungen_

Hier lassen sich die Links und Rechtswerte der X- und Y-Achsen anpassen, als auch die Achsenbeschriftungen unter Label modifizieren. _Scale_ bietet die Möglichkeit, logarithmische Achsen zu verwenden.

Im Reiter _Curves_ finden sich Einstellungsmöglichkeiten um die Linien anzupassen.

Line:
>Der unter Label vergebene Name wird in Legenden angezeigt. _Line Style_ bietet unterschiedliche Linienarten an, _Draw Style_ bietet Möglichkeiten die Linien "eckiger" darzustellen, _Width_ gibt die Strichstärke vor.

Marker:
>Unter _Style_ finden sich etliche mögliche Marker von Kreis über Viereck bis Stern deren Größe über Size einstellbar sind. Die Farbe (_Color_) kann für die Umrandung (_Edge_) und die Fläche (_Face_) angepasst werden.

**Die Pfeil-Symbole** ![](docs/source/_static/abb11.png)

Über die Pfeil-Symbole kann man zu den vorherigen oder folgenden Änderungen der Ansicht springen, bzw. Änderungen rückgängig machen.

**Das Haus-Symbol** ![](docs/source/_static/abb12.png)

Mit einem Klick auf dieses Symbol kann in die Original-Ansicht zurückgesprungen werden.

**Das Disketten-Symbol** ![](docs/source/_static/abb13.png)

Hier kann der Dialog zum Abspeichern der Abbildung aufgerufen werden. Es stehen unterschiedliche pixelbasierte und vektorbasierte Formate zur Auswahl.

## Installation


### Systemanforderungen

Die Erweiterung benötigt QGIS in der Programmversion 3.X Speziell wurde es für die Version 3.6 entwickelt und sollte daher mit dieser die höchste Kompatibilität aufweisen. Andere Programmversionen von QGIS aus der Version 3.X sollten ebenso einsetzbar sein.

### Installation weiterer Python-Pakete

Zunächst ist die OSGeo4W Shell, je nach Installationsstart von QGIS mit oder ohne Administratorrechte, zu öffnen.

Die Python3 Umgebung in der OSGeo4W Shell ist durch den Befehl

```
py3_env
```
zu erstellen.

_Easy\_setup_ von hier beziehen und entpacken:
[_Easy\_setup_](https://files.pythonhosted.org/packages/ba/2c/743df41bd6b3298706dfe91b0c7ecdc47f2dc1a3104abeb6e9aa4a45fa5d/ez_setup-0.9.tar.gz)

Als nächstes in das entpackte Verzeichnis wechseln (_cd Verzeichnis_), dann erfolgt die Installation von Installationshilfen mittels

```
python ez_setup.py -U setuptools
```

#### Matplotlib
Die Installation von _matplotlib_ (Paket zur Visualisierung) erfordert

```
easy_install matplotlib
```

#### Das Plugin QuickMapServices zur Ansicht von OpenStreetMaps Karten

Das Plugin _QuickMapServices_ ist über den Plugin-Manager von QGIS zu beziehen

>Menü : Erweiterungen : Erweiterungen verwalten und installieren

Wichtig ist unter Einstellungen das Häckchen für "Auch experimentelle Erweiterungen anzeigen" zu setzen. In der Suchleiste ist dann nach _QuickMapServices_ zu suchen.


#### Eigenes Repository

Der Quellcode des Plug-Ins ist unter dem öffentlichen Repository

https://bitbucket.org/BAH_Berlin/kostra\_tools/src/master/

zu finden.


### Installation der Erweiterung

Die Installation der QGIS-Erweiterung erfordert das Kopieren des Ordners _Kostra\_Tools_ in das
 Plug-In-Verzeichnis von QGIS. Falls dieses noch nicht existiert, ist dies zu erstellen:

>C:\Users\NUTZER\AppData\Roaming\QGIS\QGIS3\profiles\default\python\plugins

NUTZER ist dabei der Nutzername des Windows-Kontos eines Bearbeiters oder Default für alle Nutzer eines Rechners.

In QGIS selbst ist das Plug-In zu aktivieren. Im Menü unter Erweiterungen den Eintrag "Erweiterungen verwalten und installieren" auswählen. Im Dialogfenster zu "Installiert" navigieren und das Häkchen für _Kostra\_Tools_ setzen.

>Menü : Erweiterungen : Erweiterungen verwalten und installieren

Existiert der Eintrag nicht, ist noch einmal der korrekte Pfad zur Erweiterung zu prüfen oder QGIS neu zu starten, um alle neuen Erweiterungen einzulesen.

Wichtig ist unter Einstellungen das Häckchen für "Auch experimentelle Erweiterungen anzeigen" zu setzen.


#### Installation von ArcEGMO

Für die Berechnung von Bemessungshochwasser ist eine lauffähige und angepasste Programmversion von ArcEGMO© notwendig. Die Datensätze der Bemessungsniederschläge befinden sich in den Shape-Dateien in den entsprechenden Unterverzeichnissen von ArcEGMO. Eine gepackte Modellstruktur wird vom Büro für Angewandte Hydrologie bereitgestellt und ist an einen Speicherort mit lese- und schreibrechtenabzulegen, auf die von QGIS-Arbeitsplätzen Zugriff besteht.


### Konfiguration der QGIS-Erweiterung

Alle Einstellungen zur QGIS-Erweiterung LHW-Tools finden sich in der Konfigurationsdatei setup\_\_.py im Verzeichnis

>C:\Users\NUTZER\AppData\Roaming\QGIS\QGIS3\profiles\default\python\plugins\kostra\_tools\setup

Die Konfigurationsdatei ist eine Python-Skriptdatei. Als solches sind ausschließlich die Einträge rechtsseitig der Gleichheitszeichen zu verändern. Pfadangaben und Angaben zu Dateinamen, etc., wie "C:\Users\NUTZER" sind in Hochkommas anzugeben, Zahlenwerte ohne. Andernfalls verliert die Erweiterung die Lauffähigkeit.

## Quellenangaben

DWD Climate Data Center (CDC), Raster der Wiederkehrintervalle für Starkregen(Bemessungsniederschläge) in Deutschland (KOSTRA-DWD), Version 2010R.

DWD REWANUS: "Regionalisierte Extremwerte des Niederschlagsdargebots 1961/62 - 2005/06 aus Regen und Schneeschmelze für Sachsen-Anhalt" bereitgestellt über LHW am 27. Juli 2016
